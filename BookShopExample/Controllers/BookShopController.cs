﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookShopExample.Controllers
{
    public class BookShopController : Controller
    {
        // GET: BookShop

        private Models.BookShopDB db = new Models.BookShopDB();

        public ActionResult Index(int categoryId = -1, long bookId = -1)
        {
            var info = new Models.BookShopInfo()
            { 
                CategoryId = categoryId, 
                BookId = bookId,
            };
           
            return View(info);
        }

        public ActionResult BookList(int categoryId)
        {
            var books = db.Books;
            var categories = db.BookCategories;

            ViewBag.Categories = categories;

            

            return (categoryId != -1) ?
                    PartialView(books.OrderBy(a => a.Name).Where(a => a.Category == categoryId)) :
                    PartialView(books.OrderBy(a => a.Name));
        }

        [HttpGet]
        public ActionResult Choose(long bookId)
        {
            var chosenBook = db.Books.SingleOrDefault(a => a.BookId == bookId);
            ViewBag.ChosenBook = chosenBook;
            return PartialView();
        }

        [HttpPost]
        public ActionResult Choose(Models.Purchase purchase)
        {
            var chosenBook = db.Books.SingleOrDefault(a => a.BookId == purchase.BookFid);
            ViewBag.ChosenBook = chosenBook;
            return PartialView(purchase);
        }

        [HttpPost]
        public ActionResult Buy(Models.Purchase purchase)
        {
            var chosenBook = db.Books.Single(a => a.BookId == purchase.BookFid);
            ViewBag.ChosenBook = chosenBook;
            
            
            if (ModelState.IsValid)
            {
                ViewBag.CustomerName = purchase.CustomerName;
                chosenBook.Amount--;
                db.SaveChanges();
                return View();
            }
            else
            {
                //return View("Choose", new Models.Purchase() { BookFid = purchase.BookFid });
                return View("Index", new Models.BookShopInfo()
                {
                    CategoryId = -1,
                    BookId = purchase.BookFid,
                });
            }
           
        }
    }
}
