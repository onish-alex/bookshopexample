﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookShopExample.Models
{
    public class BookShopInfo
    {
        private long _bookId;

        public int CategoryId { get; set; }
        public long BookId 
        { 
            get => _bookId;
            set
            {
                _bookId = value;
                IsBuyFormVisible = (_bookId == -1) ? false : true;
            }
        }
        public bool IsBuyFormVisible { get; private set; }

    }
}